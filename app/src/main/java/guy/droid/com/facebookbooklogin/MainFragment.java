package guy.droid.com.facebookbooklogin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Mohammed Razik on 11/30/2015.
 */
public class MainFragment extends Fragment {
    TextView textView;
    ImageView imageView;
    private CallbackManager callbackManager;
    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {


        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken accessToken = loginResult.getAccessToken();
            Log.i("TOKEN", accessToken.toString());
          //  Profile profile = Profile.getCurrentProfile();
          //  displayWelcomeMessage(profile);


            GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                @Override
                public void onCompleted(JSONObject object, GraphResponse response) {
                    Log.i("LoginActivity", object.toString());
                    textView.setText("");
                    setProfileData(response.getJSONObject());

                    // Get facebook data from login
                 //   Bundle bFacebookData = getFacebookData(object);
                    Toast.makeText(getActivity().getApplicationContext(),"1"+response.getJSONObject(),Toast.LENGTH_LONG).show();

                }


        });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location,friends.limit(100)"); // Parámetros que pedimos a facebook
            request.setParameters(parameters);
            request.executeAsync();

        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException error) {

        }
    };
    public MainFragment()
    {

    }
    @Nullable
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
      /*  AccessTokenTracker tokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {

            }
        };
        ProfileTracker profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
           // displayWelcomeMessage(currentProfile);
                Toast.makeText(getActivity().getApplicationContext(),"2",Toast.LENGTH_LONG).show();
            }
        };
        tokenTracker.startTracking();
        profileTracker.startTracking();*/
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment,container,false);
        textView = (TextView)view.findViewById(R.id.results);
        imageView = (ImageView)view.findViewById(R.id.profile_picture);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LoginButton loginButton = (LoginButton)view.findViewById(R.id.facebook_button);

        List<String> permissions = new ArrayList<String>();


        permissions.add("email");
        permissions.add("user_birthday");
        permissions.add("user_friends");
        loginButton.setReadPermissions(permissions);

        
        loginButton.setFragment(this);
        loginButton.registerCallback(callbackManager,callback);
    }

    @Override
    public void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        displayWelcomeMessage(profile);
    }
    private void displayWelcomeMessage(Profile profile)
    {
    if(profile!=null)
    {
        textView.setText(profile.getFirstName()+""+profile.getLastName()+"\n"+profile.getId()+"\n");
    }
    }
    public void setProfileData(JSONObject object)
    {
        try
        {
            StringBuilder data = new StringBuilder();
            if(object.has("first_name"))
            {
                data.append(object.getString("first_name")+" ");
            }
            if(object.has("last_name"))
            {
                data.append(object.getString("last_name")+"\n");
            }
            if(object.has("id"))
            {
                data.append(object.getString("id")+"\n");
            }
            if(object.has("birthday"))
            {
                data.append(object.getString("birthday")+"\n");
            }
            if(object.has("gender"))
            {
                data.append(object.getString("gender")+"\n");
            }
            if(object.has("email"))
            {
                data.append(object.getString("email")+"\n");
            }
            if(object.has("friends"))
            {
               JSONObject friends = new JSONObject(object.getJSONObject("friends")+"");
               if(friends.has("summary"))
               {
                   JSONObject total_friends = new JSONObject(friends.getJSONObject("summary")+"");
                   if (total_friends.has("total_count"))
                   {
                       data.append("\n TOTAL FRIENDS  "+total_friends.get("total_count"));
                   }
               }
            }
            Glide.with(this).load("https://graph.facebook.com/"+object.getString("id")+"/picture?width=200&height=200").into(imageView);
            textView.setText(data);


           /* textView.setText(object.getString("first_name")+" "+object.getString("last_name")+"\n"+""+object.getString("id")
            +"\n"+object.getString("birthday")+"\n"+""+object.getString("gender")+"\n"+""+object.getString("email"));*/
        }catch (Exception e)
        {
Toast.makeText(getActivity().getApplicationContext(),""+e,Toast.LENGTH_LONG).show();
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }
}
